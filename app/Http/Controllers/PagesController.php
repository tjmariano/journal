<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    public function index(){
    	return view('pages.index');
    }
    public function about(){
    	return view('pages.about');
    }
    public function report(Request $request){
        $test = array();
        $str = explode(",",$request['print']);
        foreach($str as $id) {
            $posts = Post::where('id', $id)->first();
            array_push($test, $posts);
        }
             
              return view('pages.printPreview')->with('posts', $test);
    

}
}
