@extends('layouts.app')

@section('content')
<div class="container">
    <br>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-dark" style="color:honeydew">Dashboard</div>
                <div class="card-body">
                  
                   
                  
                        <h3> Your Tasks</h3>
                        <a href= "/posts/create" class="btn btn-primary">Create Task</a>
                        <br>
                        <br>
                    @if(count($posts)>0)
                        <table class="table table-striped" id="tblData">
                            <tr>
                                <th><input type="checkbox" name="selectAll" id="chkParent">  Select All</th>
                                <th>Title</th>
                                <th></th>
                                <th></th>
                            </tr>

                            <form action="/report">		
                                <input type="hidden" id="values" name="print">
                                
                            </form>

                            @foreach($posts as $post)
                                <tr>
                                    <td><input type="checkbox" class="checkbox" name="selectedValue[]" value="{{$post->id}}" ></td>
                                    <td>{{$post->title}}</td>
                                    <td><a href="/posts/{{$post->id}}/edit" class="btn btn-secondary">Edit</a></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </table>
                        <button type="submit" onclick="viewAll();"  class="btn btn-dark btn-sm" style="float:left";>Print</button>
                
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js" ></script>
<script type='text/javascript'>
$(document).ready(function() {
  $('#chkParent').click(function() {
    var isChecked = $(this).prop("checked");
    $('#tblData tr:has(td)').find('input[type="checkbox"]').prop('checked', isChecked);
  });

  $('#tblData tr:has(td)').find('input[type="checkbox"]').click(function() {
    var isChecked = $(this).prop("checked");
    var isHeaderChecked = $("#chkParent").prop("checked");
    if (isChecked == false && isHeaderChecked)
      $("#chkParent").prop('checked', isChecked);
    else {
      $('#tblData tr:has(td)').find('input[type="checkbox"]').each(function() {
        if ($(this).prop("checked") == false)
          isChecked = false;
      });
      console.log(isChecked);
      $("#chkParent").prop('checked', isChecked);
    }
  });
});
 function viewAll() {
    var array = [];	
    var selectedChkBox = [];
    $('.checkbox').each(function() {
        if ($(this).is(':checked')) {
               var current = $(this).val();
               selectedChkBox.push($(this).parents("tr").find(".checkbox").val())
               }
    array.push($(this).val());
    });
     $("#values").val(selectedChkBox)
   
}
</script>
@endsection
